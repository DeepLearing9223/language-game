open a shell in the singularity image (our replacement for docker)
	/share/apps/singularity/2.4.4/bin/singularity shell /beegfs/work/public/singularity/textmorph-1.2.img


TODO
	figure out why singularity image cannot see gpu 
		==> despite being on a gpu machine we are still training on cpu
			why?
				I think we need to install drivers / cudnn etc on singularity image, but that's
				wierd because Lucia said she was training on gpu using this image
				
				Might email her again, but don't want to bother her.

				The other researcher responded

				we can scheduale a consult with HPC if we neeed
					email: hpc@nyu.edu
