from __future__ import division
'''
This is where the shared endcoder-decoder pair will live.
The players will produce edit vectors (actions) which the board will
will apply to its current state (sentance) to transition to the next
state (edited sentance)
'''

from neural_editor.textmorph.edit_model.training_run import EditTrainingRuns
from neural_editor.textmorph import data

from textmorph.edit_model.editor import EditExample

from nltk import word_tokenize



#import cPickle as pickle



#DATA_DIR = "/scratch/jl4722/deep_learning_proj/project/AlphaLang/pulled_codes/alpha_zero_general_cpy/language/neural_editor/data"
# DATA_DIR = "/scratch/jl4722/deep_learning_proj/fresh_start/project/AlphaLang/pulled_codes/alpha_zero_general_cpy/language/neural_editor"

experiments = EditTrainingRuns(check_commit=(False))

RUN_NUM = 25

# print("{}{}{}".format(DATA_DIR, "/edit_runs/data/", RUN_NUM))

# exp = experiments.get_by_path("{}{}{}".format(DATA_DIR, "/edit_runs/", RUN_NUM))

exp = experiments.get(RUN_NUM)
editor = exp.editor

# print(editor)

# pickle.dump(editor, open('editor.p', 'wb'))

# editor = pickle.load(open('editor.p', 'rb'))
# print(editor)


# from ..neural-editor-cpy.textmorph.edit_model.encoder import Encoder
# from ..neural-editor-cpy.gtd.ml.torch.source_encoder import *


# encoder = 

# for now this is coppied from 
# othello game by
#     Eric P. Nichols
#     Feb 8, 2008.
# but most of this code is othello specific and just serves as a skeliton for now
# 
# 
# Board class.
# Board data:
#   1=white, -1=black, 0=empty
#   first dim is column , 2nd is row:
#      pieces[1][7] is the square in column 2,
#      at the opposite end of the board in row 8.
# Squares are stored and manipulated as (x,y) tuples.
# x is the column, y is the row.

class Board():

    # list of all 8 directions on the board, as (x,y) offsets
    __directions = [(1,1),(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1)]

    def __init__(self, input_sentance="the food is good but bad service."):
        # "Set up initial board configuration."

        # this should be a sentance sampled from the training set
        # or input by the user 
        self.input_sentance = input_sentance.decode("utf-8")

        self.source_words  = word_tokenize(self.input_sentance)
        
        self.ex = EditExample(self.source_words, [], [], [], [], [])

#        self.pieces = editor.preprocess([ex])
#        self.pieces = self.ex
        
#        self.n = n
#        # Create the empty board array.
#        self.pieces = [None]*self.n
#        for i in range(self.n):
#            self.pieces[i] = [0]*self.n
#
#        # Set up the initial 4 pieces.
#        self.pieces[int(self.n/2)-1][int(self.n/2)] = 1
#        self.pieces[int(self.n/2)][int(self.n/2)-1] = 1
#        self.pieces[int(self.n/2)-1][int(self.n/2)-1] = -1;
#        self.pieces[int(self.n/2)][int(self.n/2)] = -1;


    # add [][] indexer syntax to the Board
    def __getitem__(self, index): 
        return self.ex[index]

    def countDiff(self, color):
        # TODO: this gets used in scoring 
        # maybe we should calculate our
        # differance funciton here to
        # (bleu, jackard, w/e)

        """Counts the # pieces of the given color
        (1 for white, -1 for black, 0 for empty spaces)"""
        count = 0
        for y in range(self.n):
            for x in range(self.n):
                if self[x][y]==color:
                    count += 1
                if self[x][y]==-color:
                    count -= 1
        return count

    def get_legal_moves(self, color):
        """Returns all the legal moves for the given color.
        (1 for white, -1 for black
        """
        moves = set()  # stores the legal moves.

        # Get all the squares with pieces of the given color.
        for y in range(self.n):
            for x in range(self.n):
                if self[x][y]==color:
                    newmoves = self.get_moves_for_square((x,y))
                    moves.update(newmoves)
        return list(moves)

#    def has_legal_moves(self, color):
#        for y in range(self.n):
#            for x in range(self.n):
#                if self[x][y]==color:
#                    newmoves = self.get_moves_for_square((x,y))
#                    if len(newmoves)>0:
#                        return True
#        return False

#    def has_legal_moves(self, color):
#        ## think about what this should mean
#        # perhaps this is where to implement edit budget
#        
#        return True

    def get_moves_for_square(self, square):
        """Returns all the legal moves that use the given square as a base.
        That is, if the given square is (3,4) and it contains a black piece,
        and (3,5) and (3,6) contain white pieces, and (3,7) is empty, one
        of the returned moves is (3,7) because everything from there to (3,4)
        is flipped.
        """
        (x,y) = square

        # determine the color of the piece.
        color = self[x][y]

        # skip empty source squares.
        if color==0:
            return None

        # search all possible directions.
        moves = []
        for direction in self.__directions:
            move = self._discover_move(square, direction)
            if move:
                # print(square,move,direction)
                moves.append(move)

        # return the generated move list
        return moves

    def execute_move(self, move, color):
        # TODO: move is an edit vector, color will indicate 
        # which player is making the move (corrupter / correcter )
        # the encoder-decoder pair will apply the move (edit) to the current
        # state (sentance)

        """Perform the given move on the board; flips pieces as necessary.
        color gives the color pf the piece to play (1=white,-1=black)
        """

        #Much like move generation, start at the new piece's square and
        #follow it on all 8 directions to look for a piece allowing flipping.

        # Add the piece to the empty square.
        # print(move)
        # flips = [flip for direction in self.__directions
        #               for flip in self._get_flips(move, direction, color)]
        # assert len(list(flips))>0
        # for x, y in flips:
        #     #print(self[x][y],color)
        #     self[x][y] = color
        
        assert type(move).__name__ == EditExample.__name__
        
        beams, traces = editor.edit([move])
        # print(self.ex)
        self.ex = EditExample(beams[0][0], [], [], [], [], [])
        # print(self.ex)

    def _discover_move(self, origin, direction):
        """ Returns the endpoint for a legal move, starting at the given origin,
        moving by the given increment."""
        x, y = origin
        color = self[x][y]
        flips = []

        for x, y in Board._increment_move(origin, direction, self.n):
            if self[x][y] == 0:
                if flips:
                    # print("Found", x,y)
                    return (x, y)
                else:
                    return None
            elif self[x][y] == color:
                return None
            elif self[x][y] == -color:
                # print("Flip",x,y)
                flips.append((x, y))

    def _get_flips(self, origin, direction, color):
        """ Gets the list of flips for a vertex and direction to use with the
        execute_move function """
        #initialize variables
        flips = [origin]

        for x, y in Board._increment_move(origin, direction, self.n):
            #print(x,y)
            if self[x][y] == 0:
                return []
            if self[x][y] == -color:
                flips.append((x, y))
            elif self[x][y] == color and len(flips) > 0:
                #print(flips)
                return flips

        return []

    @staticmethod
    def _increment_move(move, direction, n):
        # print(move)
        """ Generator expression for incrementing moves """
        move = list(map(sum, zip(move, direction)))
        #move = (move[0]+direction[0], move[1]+direction[1])
        while all(map(lambda x: 0 <= x < n, move)): 
        #while 0<=move[0] and move[0]<n and 0<=move[1] and move[1]<n:
            yield move
            move=list(map(sum,zip(move,direction)))
            #move = (move[0]+direction[0],move[1]+direction[1])

