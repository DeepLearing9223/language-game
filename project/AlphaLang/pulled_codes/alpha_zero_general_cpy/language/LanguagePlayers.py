from __future__ import division # TODO: do we need this? 
                                # int(i/self.game.n) does that imply yes? i think so...
import numpy as np
import torch

from nltk import word_tokenize
from language.neural_editor.textmorph.edit_model.editor import EditExample

from gtd.ml.torch.utils import GPUVariable

class RandomPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
	# TODO: make this sample from vMF(0)

        norm = torch.rand(1)*10 
        print(norm)
        rand_vec = torch.randn(128)
        norm = norm.expand_as(rand_vec)
        edit_vector = norm * (rand_vec / torch.norm(rand_vec, 2))
        
        move = EditExample(
            source_words=board.source_words, 
            insert_words=[], insert_exact_words=[], 
            delete_words=[], delete_exact_words=[], 
            target_words=[],
            edit_embed=GPUVariable(edit_vector)   
        )
        return move

class HumanLanguagePlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        # TODO: allow player to specify norm
        get_input = lambda prompt: word_tokenize(raw_input(prompt).decode('utf-8'))

        source_words = board.source_words
        whitelist = sorted(get_input('Enter insert words (OK to leave empty):\n'))
        blacklist = sorted(get_input('Enter delete words (OK to leave empty):\n'))
        # target_words = get_input('Enter target sentence (OK to leave empty):\n')

        move = EditExample(source_words, whitelist, [], blacklist, [], [])
        return move
    

class GreedyLanguagePlayer():
    # TODO: adapt for language 
    def __init__(self, game):
        self.game = game

    def play(self, board):
        valids = self.game.getValidMoves(board, 1)
        candidates = []
        for a in range(self.game.getActionSize()):
            if valids[a]==0:
                continue
            nextBoard, _ = self.game.getNextState(board, 1, a)
            score = self.game.getScore(nextBoard, 1)
            candidates += [(-score, a)]
        candidates.sort()
        return candidates[0][1]
