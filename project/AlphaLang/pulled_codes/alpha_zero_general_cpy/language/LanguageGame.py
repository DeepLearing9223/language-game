from __future__ import division
from __future__ import print_function
import sys
sys.path.append('..')
from Game import Game
from .LanguageLogic import Board
import numpy as np

from copy import deepcopy

import torch
from textmorph.edit_model.editor import EditExample
from gtd.ml.torch.utils import GPUVariable


import numpy as np


#action_dict = GPUVariable(torch.eye(self.action_size)).cuda()
action_dict = GPUVariable(torch.eye(128)).cuda()

class LanguageGame(Game):
    def __init__(self, n):
        self.n = n
        
        


    def getInitBoard(self):
        # return initial board 
        """
        Returns:
            startBoard: a representation of the board (ideally this is the form
                        that will be the input to your neural network)
        """
        get_input = lambda prompt: word_tokenize()
        input_text = raw_input("enter input sentence (ok to leave empty)\n")
        b = Board() if input_text=="" else Board(input_text.decode('utf-8'))
        return b.ex

    def getBoardSize(self):
        # (a,b) tuple

        # TODO: this is probably the 
        # number of horizontal gridlines
        # by the number of vertical gridlines
        # around the edit sphere once we discritize 
        # it but is really infinity
        # because all points on the 
        # sphere are valid actions

        # n is the number of lattice points on the sphere 
        
        return self.n

    def getActionSize(self):
        # return number of actions

        # TODO: this is probably the 
        # number of lattice points on
        # the surface of the edit
        # sphere once we discritize it, 
        # but is really infinity
        # because all points on the 
        # sphere are valid actions

        return self.n



    def getNextState(self, board, player, action):
        # if player takes action on board, return next (board,player)
        # action must be a valid move
        """
        Input:
            board: current board
            player: current player (1 or -1)
            action: action taken by current player

        Returns:
            nextBoard: board after applying action
            nextPlayer: player who plays in the next turn (should be -player)
        """

        # TODO: in the case of the edit model board will contain the current 
        # sentance and the action will be the edit vector (z) produced 
        # by player. When we call b.execute_move the board will use 
        # the shared encoder-decoder pair to apply the edit z

        # if action == self.n*self.n:
        #     return (board, -player)
        # b = Board(self.n)
        # b.pieces = np.copy(board)
        # move = (int(action/self.n), action%self.n)
        # b.execute_move(move, player)
        # return (b.pieces, -player)
    
        b = Board()
        b.ex = deepcopy(board)
        
        print("NEXTSTATE::: action:", action)
        
        print(type(board).__name__,type(action).__name__)
        if type(action).__name__ is not type(board).__name__:
            move = EditExample(
                source_words=board.source_words, 
                insert_words=[], insert_exact_words=[], 
                delete_words=[], delete_exact_words=[], 
                target_words=[],
                edit_embed=action_dict[action]
            )
        else: 
            move = action
        
        
        
        b.execute_move(move, player)
        
        return (b.ex, -player)
    
        

    def getValidMoves(self, board, player):
        # return a fixed size binary vector

        # TODO: this may actutually be easier than we thought. 
        # because both players are sharing the board/encoder-decoder
        # any z they produce will be valid so this can just be the identity
        
        # However, in the case where each player is allowed a fixed number 
        # of edits or a fixed length of edit vector perhaps a check of
        # those constraints could be done here.

        # also could implement a wierd form of dropout where we mask
        # certain parts of the action space despite those moves 
        # being legal and force our algorithm to choose among the 
        # remaining options as perhaps both a regularizer, and 
        # to encourage exploration

        # valids = [0]*self.getActionSize()
        # b = Board(self.n)
        # b.pieces = np.copy(board)
        # legalMoves =  b.get_legal_moves(player)
        # if len(legalMoves)==0:
        #     valids[-1]=1
        #     return np.array(valids)
        # for x, y in legalMoves:
        #     valids[self.n*x+y]=1
        # return np.array(valids)
    
        # this creates a new instnace of a new class called Always1 which 
        # when indexed using __getitem__ or the [] syntax will always return 1
        # and when used in a product will behave as the identity
        valids = type('Always1', (object,), {
            "__getitem__": lambda self, key: 1,
            "__mul__": lambda self, other: other,
            "__rmul__": lambda self, other: other
        })()  
        
        return valids

    def getGameEnded(self, board, player):
        # return 0 if not ended, 1 if player 1 won, -1 if player 1 lost
        # player = 1
        """
        Input:
            board: current board
            player: current player (1 or -1)

        Returns:
            r: 0 if game has not ended. 1 if player won, -1 if player lost,
               small non-zero value for draw.
               
        """

        # TODO: still not 100% sure on this. Maybe games are of fixed 
        # length when the game ends scoreing happens here. Maybe each
        # player has a certain total length of edit vector they can
        # produce and once both players have either passed or used up
        # all their edit length the game ends and is scored based on the
        # differance between source and target, and edit distance
        # applied by each player 

        # b = Board(self.n)
        # b.pieces = np.copy(board)
        # if b.has_legal_moves(player):
        #     return 0
        # if b.has_legal_moves(-player):
        #     return 0
        # if b.countDiff(player) > 0:
        #     return 1
        # return -1
        
        #return 0
        #print np.random.choice([0, 1e-2, 1, -1], p=[.9, .04, .03, .03])
    
    
        ### for now just decide on a winner at random
        
        result = np.random.choice([0, 1e-4, 1, -1], p=[.5, .2, .15, .15])
        print (result)
        return result
                                
    def getCanonicalForm(self, board, player):
        # return state if player==1, else return -state if player==-1

        # this flips the perspective on the board i.e. sets all
        # black pieces to white and all white pieces to black. 

        # TODO: I'm not 100% sure how we should handle this, but 
        # if we are playing the corrupt and recover game and we 
        # assume the state includes whether the player to move 
        # is the corrupter or the correcter we should atleast flip
        # the bit that encodes that.

        """
        Input:
            board: current board
            player: current player (1 or -1)

        Returns:
            canonicalBoard: returns canonical form of board. The canonical form
                            should be independent of player. For e.g. in chess,
                            the canonical form can be chosen to be from the pov
                            of white. When the player is white, we can return
                            board as is. When the player is black, we can invert
                            the colors and return the board.
        """

        # I think we should setup a funciton like board.flip and we'll
        # keep one additional int which is 1 for p1 and -1 for p2 
        # connected to the board state
        
        # return player*board
        return board

    def getSymmetries(self, board, pi):
        # mirror, rotational

        # ... 
        """
        Input:
            board: current board
            pi: policy vector of size self.getActionSize()

        Returns:
            symmForms: a list of [(board,pi)] where each tuple is a symmetrical
                       form of the board and the corresponding pi vector. This
                       is used when training the neural network from examples.
        """

        assert(len(pi) == self.n**2+1)  # 1 for pass
        pi_board = np.reshape(pi[:-1], (self.n, self.n))
        l = []

        for i in range(1, 5):
            for j in [True, False]:
                newB = np.rot90(board, i)
                newPi = np.rot90(pi_board, i)
                if j:
                    newB = np.fliplr(newB)
                    newPi = np.fliplr(newPi)
                l += [(newB, list(newPi.ravel()) + [pi[-1]])]
        return l

    def stringRepresentation(self, board):
        # 8x8 numpy array (canonical board)

        # this is the natural representation of our data

        """
        Input:
            board: current board

        Returns:
            boardString: a quick conversion of board to a string format.
                         Required by MCTS for hashing.
        """

        return board.tostring()

    def getScore(self, board, player):
        # TODO: for corrupt-correct maybe the bleu 
        # score between the start sentance and the 
        # ending sentance where the correcting 
        # player is trying to maximize the score (overlap)
        # while traversing the minimum distance through
        # edit space. In this formulation the two players
        # are competing to find shortest paths along the
        # data manafold of sentances with the corrupting 
        # player trying to push the final sentance as
        # as far away from the start as possible using
        # the minimal amount of edit and then the
        # correcting player trying to get back
        # using less edit than the corrupting
        # player used.

        b = Board(self.n)
        b.ex = np.copy(board)
        return b.countDiff(player)

def display(board):

    # TODO: this should probably just print the string.
    print(board)
    
    
#   print(board.input_sentance)
#   print(board.source_words)
#   print(board.ex)
#   print(board.pieces)
#   
#   
#   
#    n = board.shape[0]
#
#    for y in range(n):
#        print (y,"|",end="")
#    print("")
#    print(" -----------------------")
#    for y in range(n):
#        print(y, "|",end="")    # print the row #
#        for x in range(n):
#            piece = board[y][x]    # get the piece to print
#            if piece == -1: print("b ",end="")
#            elif piece == 1: print("W ",end="")
#            else:
#                if x==n:
#                    print("-",end="")
#                else:
#                    print("- ",end="")
#        print("|")
#
#    print("   -----------------------")
