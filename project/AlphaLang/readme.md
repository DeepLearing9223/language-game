## quickstart

To play a language game between two human players navigate to 
the [alpha-zero folder](pulled_codes/alpha_zero_general_cpy) and run 

`./play.sh`


## jupyter
there are also several examples in 
[this](pulled_codes/alpha_zero_general_cpy/Example%20Games.ipynb) notebook
 

## TODO
 - finish implementing nn player
 - finish modifing MCTS
 - finish compiling GEC data
 - retrain on GEC data
  
